/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/
#pragma once

// will probably be useful if someone's using the math library
#include <cmath> 
#include <vector>

/*
I find the best approach to a Linear algebra style library is
to be as functional as possible. That means POD, and lots of
overloads. These could be done using templates, and arguments
can be made to do so, but I find that the increase in compile
time is disproportionate to any gains from templates.
Also could be seen as an "exercise to the reader" to templatize
some of the functions.
*/
namespace math {
	// Floating point precision error tolerance
	static float const Epsilon = .1e-5F ;


	/**********************************************************
	Type forward declaration
	*/
	struct Vec2 ;
	struct Vec3 ;
	struct Vec4 ;
	struct Mat2 ;
	struct Mat3 ;
	struct Mat4 ;

	typedef std::vector<Vec2> ConvexHull2D ;
	typedef std::vector<Vec3> ConvexHull3D ;
	typedef std::vector<Vec4> ConvexHull ;

	/**********************************************************
	Function declarations
	*/
	///////////////////////////////////////////////////////////
	// Section: Useful conversions
	Vec3 Promote(Vec2 const &v) ;
	Vec4 Promote(Vec3 const &v) ;
	Mat3 Promote(Mat2 const &m) ;
	Mat4 Promote(Mat3 const &m) ;

	///////////////////////////////////////////////////////////
	// Section: Unary functions
	Vec2 operator-(Vec2 const &v) ;
	Vec3 operator-(Vec3 const &v) ;
	Vec4 operator-(Vec4 const &v) ;

	Vec2 LengthSq(Vec2 const &v) ;
	Vec3 LengthSq(Vec3 const &v) ;
	Vec4 LengthSq(Vec4 const &v) ;
	Vec2 Length(Vec2 const &v) ;
	Vec3 Length(Vec3 const &v) ;
	Vec4 Length(Vec4 const &v) ;

	Vec2 Normal(Vec2 const &v) ;
	Vec3 Normal(Vec3 const &v) ;
	Vec4 Normal(Vec4 const &v) ;

	///////////////////////////////////////////////////////////
	// Section: Creation functions
	Mat2 Rotate2D(float rads) ;
	Mat3 Rotate3D(float rads, Vec3 const &axis) ;

	Mat3 Translate(Vec2 const &t) ;
	Mat4 Translate(Vec3 const &t) ;

	Mat2 Scale2D(float uniform) ;
	Mat3 Scale3D(float uniform) ;
	Mat4 Scale4D(float uniform) ;

	Mat2 Scale2D(float x, float y) ;
	Mat3 Scale3D(float x, float y, float z) ;
	Mat4 Scale4D(float x, float y, float z, float w) ;

	Mat2 Scale2D(Vec2 const &v) ;
	Mat3 Scale3D(Vec3 const &v) ;
	Mat4 Scale4D(Vec4 const &v) ;

	///////////////////////////////////////////////////////////
	// Section: Side effect operators
	Vec2 &operator+=(Vec2 &v1, Vec2 const &v2) ;
	Vec3 &operator+=(Vec3 &v1, Vec3 const &v2) ;
	Vec4 &operator+=(Vec4 &v1, Vec4 const &v2) ;

	Vec2 &operator-=(Vec2 &v1, Vec2 const &v2) ;
	Vec3 &operator-=(Vec3 &v1, Vec3 const &v2) ;
	Vec4 &operator-=(Vec4 &v1, Vec4 const &v2) ;

	Vec2 &operator*=(Vec2 &v1, float scalar) ;
	Vec3 &operator*=(Vec3 &v1, float scalar) ;
	Vec4 &operator*=(Vec4 &v1, float scalar) ;

	Vec2 &operator/=(Vec2 &v1, float scalar) ;
	Vec3 &operator/=(Vec3 &v1, float scalar) ;
	Vec4 &operator/=(Vec4 &v1, float scalar) ;

	Vec2 &Normalize(Vec2 &v) ;
	Vec3 &Normalize(Vec3 &v) ;
	Vec4 &Normalize(Vec4 &v) ;

	///////////////////////////////////////////////////////////
	// Section: Binary operators
	Vec2 operator+(Vec2 const &v1, Vec2 const &v2) ;
	Vec3 operator+(Vec3 const &v1, Vec3 const &v2) ;
	Vec4 operator+(Vec4 const &v1, Vec4 const &v2) ;

	Mat2 operator+(Mat2 const &m1, Mat2 const &m2) ;
	Mat3 operator+(Mat3 const &m1, Mat3 const &m2) ;
	Mat4 operator+(Mat4 const &m1, Mat4 const &m2) ;

	Vec2 operator-(Vec2 const &v1, Vec2 const &v2) ;
	Vec3 operator-(Vec3 const &v1, Vec3 const &v2) ;
	Vec4 operator-(Vec4 const &v1, Vec4 const &v2) ;

	Vec2 operator*(Vec2 const &v, float scalar) ;
	Vec3 operator*(Vec3 const &v, float scalar) ;
	Vec4 operator*(Vec4 const &v, float scalar) ;

	Vec2 operator*(Mat2 const &m, Vec2 const &v) ;
	Vec3 operator*(Mat3 const &m, Vec3 const &v) ;
	Vec4 operator*(Mat4 const &m, Vec4 const &v) ;

	Mat2 operator*(Mat2 const &m1, Mat2 const &m2) ;
	Mat3 operator*(Mat3 const &m1, Mat3 const &m2) ;
	Mat4 operator*(Mat4 const &m1, Mat4 const &m2) ;

	Vec2 operator/(Vec2 const &v, float scalar) ;
	Vec3 operator/(Vec3 const &v, float scalar) ;
	Vec4 operator/(Vec4 const &v, float scalar) ;

	Vec2 operator*(float scalar, Vec2 const &v) ;
	Vec3 operator*(float scalar, Vec3 const &v) ;
	Vec4 operator*(float scalar, Vec4 const &v) ;

	///////////////////////////////////////////////////////////
	// Section: Useful common problem functions
	Vec2 FastRotate2D(float rads, Vec2 const &v) ;
	Vec2 Calculate2DNormal(Vec2 const &v1, Vec2 const &v2) ;
	bool SATSolver2D(ConvexHull2D const &obj1, ConvexHull2D const &obj2) ;

	/**********************************************************
	Type definitions
	*/
	struct Vec2 {
		union {
			struct {
				float x, y ;
			};
			struct {
				float u, v ;
			};
			float array[2] ;
		};

		Vec2(float x, float y) : x(x), y(y) {};
		Vec2() : x(0.0F), y(0.0F) {};
	};

	struct Vec3 {
		union {
			struct {
				float x, y, z ;
			};
			struct {
			};
			float array[3] ;
		};
		Vec3(float x, float y, float z = 0.0F): x(x), y(y), z(z) {}
				float r, g, b ;
		Vec3(): x(0.0F), y(0.0F), z(0.0F) {}
		Vec3(Vec2 const &v, float z = 0.0F): x(v.x), y(v.y), z(z) {}

		Vec2 xy() const {return Vec2(x, y);};
	};

	struct Vec4 {
		union {
			struct {
				float x, y, z, w ;
			};
			struct {
				float r, g, b, a ;
			};
			float array[4] ;
		};
		Vec4(float x, float y, float z, float w = 0.0F): x(x),y(y),z(z),w(w) {}
		Vec4(Vec3 const &v, float w = 0.0F): x(v.x), y(v.y), z(v.z), w(w) {}
		Vec4(): x(0),y(0),z(0),w(0) {}

		Vec3 xyz() const {return Vec3(x,y,z);}
		Vec2 xy() const {return Vec2(x,y);}
	};
}
