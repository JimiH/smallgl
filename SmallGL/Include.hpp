/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/
#pragma once

#if defined _WIN32
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#define NOMINMAX
#include <windows.h>
#else
#include <iostream>
#endif

// Typedefs
typedef unsigned uint ;
typedef unsigned char byte ;
typedef signed char sbyte ;

// Assert + Error if
template < typename T >
T &Assert(T &what, char const *msg = "Assertion!") {
	if (!what) {
#if defined _WIN32
		MessageBoxA(nullptr, msg, msg, MB_OK) ;
		__debugbreak() ;
#else
		std::cout << msg << std::endl ;
		// break?
#endif
	}
}

template < typename T >
T const&Assert(T const&what, char const *msg = "Assertion!") {
	if (!what) {
#if defined _WIN32
		MessageBoxA(nullptr, msg, msg, MB_OK) ;
		__debugbreak() ;
#else
		std::cout << msg << std::endl ;
		// break?
#endif
	}
}

template < typename T >
T &ErrorIf(T &what, char const *msg = "Assertion!") {
	if (what) {
#if defined _WIN32
		MessageBoxA(nullptr, msg, msg, MB_OK) ;
		__debugbreak() ;
#else
		std::cout << msg << std::endl ;
		// break?
#endif
	}
}

template < typename T >
T const&ErrorIf(T const&what, char const *msg = "Assertion!") {
	if (what) {
#if defined _WIN32
		MessageBoxA(nullptr, msg, msg, MB_OK) ;
		__debugbreak() ;
#else
		std::cout << msg << std::endl ;
		// break?
#endif
	}
}
